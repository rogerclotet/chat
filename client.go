package main

import (
	"github.com/gorilla/websocket"
	"net/http"
	log "github.com/Sirupsen/logrus"
	"os/signal"
	"os"
	"syscall"
	"fmt"
)

func main() {
	dialer := websocket.Dialer{}
	conn, _, err := dialer.Dial("ws://localhost:8000/", http.Header{})
	if err != nil {
		log.WithError(err).Error("Error dialing")
	}

	signals := make(chan os.Signal)
	signal.Notify(signals, syscall.SIGINT, syscall.SIGTERM)
	go func () {
		<-signals
		os.Exit(0)
	}()

	go receive(conn)

	for {
		var input string
		fmt.Scanln(&input)

		err = conn.WriteMessage(websocket.TextMessage, []byte(input))
		if err != nil {
			log.WithError(err)
		}
	}
}

func receive(conn *websocket.Conn) {
	for {
		_, p, err := conn.ReadMessage()
		if err != nil {
			log.WithError(err)
		}

		fmt.Println(string(p))
	}
}
