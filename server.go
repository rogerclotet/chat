package main

import (
	"os/signal"
	"os"
	log "github.com/sirupsen/logrus"
	"time"
	"net/http"
	"github.com/gorilla/websocket"
)

func main() {
	signals := make(chan os.Signal)
	signal.Notify(signals)

	s := newServer()
	http.Handle("/", s)
	go http.ListenAndServe(":8000", nil)

	log.WithField("time", time.Now().String()).Info("Server started")

	<-signals
	os.Exit(0)
}

func newServer() *server {
	return &server{
		connections: []*websocket.Conn{},
	}
}

type server struct {
	connections []*websocket.Conn
}

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

func (s *server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println(err)
		return
	}

	s.connect(conn)

	for {
		messageType, p, err := conn.ReadMessage()
		if err != nil {
			log.WithError(err)
			return
		}

		log.WithField("message", string(p)).WithField("type", messageType).Info("Message received")

		s.broadcast(p)
	}
}

func (s *server) connect(conn *websocket.Conn) {
	s.connections = append(s.connections, conn)
}

func (s *server) broadcast(message []byte) {
	for _, conn := range s.connections {
		err := conn.WriteMessage(websocket.TextMessage, message)
		if err != nil {
			log.WithError(err)
		}
	}
}
